package sg.ncs.product.nxp_test_0514.web.web.controller;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TestVO {

    @ApiModelProperty("name")
    private String name;
}
